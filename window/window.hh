#ifndef __WINDOW_HH__
#define __WINDOW_HH__

#include "prelude.hh"

#define WINDOW_METHOD_XLIB	1

#if WINDOW_METHOD == WINDOW_METHOD_XLIB
#include "xlib.hh"
#endif

#endif
