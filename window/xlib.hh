#ifndef __WINDOW_XLIB_HH__
#define __WINDOW_XLIB_HH__

#include "prelude.hh"
#include <iostream>		/* For output messages			*/
#include <X11/Xlib.h>	/* For almost everything Xlib	*/
#include <X11/Xutil.h>	/* For XTextProperty 			*/

namespace window{
	namespace xlib{
		class XlibWindow: public IWindow{
		protected:
			/* Xlib variables */
			Display *display;
			int screen;
			Window window;
			Visual *visual;
			int depth;
			GC gc;

			Atom WM_DELETE_WINDOW;
			
			/* Window state variables */
			bool _closed;
			bool _reconfigured;

			Pixmap pixmap;
		public:
			XlibWindow(const char* display_name, 
					int width,
					int height);
			virtual ~XlibWindow();

			virtual signed int x();
			virtual signed int y();
			virtual unsigned int w();
			virtual unsigned int h();

			virtual bool reconfigured();
			virtual bool closed();
			virtual bool supports_alpha();
			virtual bool is_focused();

			virtual void set_title(const std::string&);
			virtual void lock_cursor(bool);
			
			virtual void paint(const Pixmap&);
			virtual void update();
		};
	}
}

#endif
