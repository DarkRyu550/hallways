#include "xlib.hh"
using namespace window::xlib;

XlibWindow::XlibWindow(const char *display_name,
		int width, int height)
{
	this->_reconfigured = false;
	this->_closed = false;

	/* Open the X display and get the default screen */
	this->display = XOpenDisplay(display_name);
	if(this->display == NULL){
		std::cerr << "[xlib::XlibWindow] Could not open Display." << std::endl;
		throw 1;
	}
	this->screen = DefaultScreen(this->display);

	/* In order to get transparency, we need to query
	 * for the TrueColor visual info. */
	XVisualInfo vinfo;
	int matched = XMatchVisualInfo(this->display, this->screen, 32, TrueColor, &vinfo);

	/* Determine the visual and depth to be used */
	this->visual = matched ? vinfo.visual : DefaultVisual(this->display, this->screen);
	this->depth  = matched ? vinfo.depth  : DefaultDepth(this->display, this->screen);

	/* Create a new window */
	XSetWindowAttributes attr;
	attr.border_pixel = 0;
	attr.background_pixel = 0;
	
	/* Attach a colormap if we matched */
	if(matched){
		attr.colormap = XCreateColormap(
				this->display,
				RootWindow(this->display, this->screen),
				vinfo.visual,
				AllocNone
				);
	}

	this->window = XCreateWindow(
			this->display,
			RootWindow(this->display, this->screen),
			0,				/* No default X position */
			0,				/* No default Y position */
			width,			/* Request the specified width */
			height,			/* Request the specified height */
			0,				/* No border */
			/* Use the matched TrueColor vinfo's depth, or
			 * copy the parent's depth if none were matched. */
			this->depth,
			InputOutput,
			/* Use the matched TrueColor vinfo's visual, or
			 * copy the parent's visual if none were matched. */
			this->visual,
			CWBackPixel | CWBorderPixel | (matched ? CWColormap : 0),
			&attr
			);

	/* Create a Graphics Context for it */
	XGCValues gcv;
	gcv.foreground = WhitePixel(this->display, this->screen);
	gcv.background = 0;

	this->gc = XCreateGC(
			this->display,
			this->window,
			GCBackground | GCForeground,
			&gcv
			);

	/* Enable handling WM_DELETE_WINDOW */
	this->WM_DELETE_WINDOW = XInternAtom(this->display, "WM_DELETE_WINDOW", false);
	XSetWMProtocols(this->display, this->window, &this->WM_DELETE_WINDOW, 1);

	/* Enable recieving and window size and events */
	XSelectInput(this->display, this->window, StructureNotifyMask | ExposureMask);

	/* Map the window */
	XMapWindow(this->display, this->window);
}

XlibWindow::~XlibWindow()
{
	XFreeGC(this->display, this->gc);
	XDestroyWindow(this->display, this->window);
	XCloseDisplay(this->display);
}

void
XlibWindow::set_title(std::string const& title)
{
	XTextProperty prop;

	char* string = const_cast<char*>(title.c_str());
	XStringListToTextProperty(&string, 1, &prop);

	static Atom WM_TITLE = XInternAtom(this->display, "WM_TITLE", false);
	static Atom WM_ICON_TITLE = XInternAtom(this->display, "WM_ICON_TITLE", false);
	static Atom WM_NAME = XInternAtom(this->display, "WM_NAME", false);

	XSetTextProperty(this->display, this->window, &prop, WM_TITLE);
	XSetTextProperty(this->display, this->window, &prop, WM_ICON_TITLE);
	XSetTextProperty(this->display, this->window, &prop, WM_NAME);
}

void
XlibWindow::update()
{
	XEvent ev;
	while(XPending(this->display)){
		XNextEvent(this->display, &ev);
		switch(ev.type){
		case ConfigureNotify:
			this->_reconfigured = true;
			break;
		case ClientMessage:
			if(ev.xclient.data.l[0] == this->WM_DELETE_WINDOW)
				this->_closed = true;

			break;
		}
	}
}

void
XlibWindow::paint(const Pixmap& pixmap)
{
	/* Create an XImage */
	XImage *image;
	image = XCreateImage(
			this->display,
			this->visual,
			this->depth,
			ZPixmap,
			0,	/* No offset at the beggining of scanlines */
			(char*) pixmap.data,
			pixmap.width,
			pixmap.height,
			32,	/* Any picture in RGBA8888 should be alignable to 32 */
			0	/* Just let Xlib calculate how many bytes are in each line */
			);

	if(image ==NULL){
		std::cerr << "[XlibWindow] Failed to create XImage." << std::endl;
		return;
	}

	image->data = (char*) pixmap.data;

	/* Display the image */
	XPutImage(
			this->display,
			this->window,
			this->gc,
			image,
			0, 0, 0, 0,
			pixmap.width,
			pixmap.height
			);

	/* Since Xlib frees the memory associated with the image
	 * data upon deletion of the XImage object, set the data
	 * pointer in the struct to NULL, as if it had never had
	 * image data associated with it, and leave the actual
	 * freeing to the allocator. */
	image->data = NULL;
	XDestroyImage(image);

	XFlush(this->display);
	XSync(this->display, false);
}

void
XlibWindow::lock_cursor(bool lock)
{
	if(lock){
		XGrabPointer(this->display, this->window,
				false, 0,		/* No need to process events */
				GrabModeSync,	/* Freeze the mouse for other clients */
				GrabModeAsync,	/* Leave the keyboard processing alone */
				this->window,	/* Confine the cursor to this window */
				None,			/* Specify no cursor */
				CurrentTime		/* Just use the server-side timestamp */
				);
	}else{
		XUngrabPointer(this->display, CurrentTime);
	}
}

bool
XlibWindow::is_focused()
{
	Window focus;
	int revert;
	XGetInputFocus(this->display, &focus, &revert);
	
	return focus == this->window;
}

/* Generic macro for getting window geometry */
#define GET_WINDOW_GEOMETRY(d, w) \
	Window _root_window; \
	int _x, _y; \
	unsigned int _width, _height, _border_width, _depth; \
	XGetGeometry(d, w, \
			&_root_window, &_x, &_y, &_width,\
			&_height, &_border_width, &_depth);

int
XlibWindow::x()
{
	GET_WINDOW_GEOMETRY(this->display, this->window);
	return _x;
}

int
XlibWindow::y()
{
	GET_WINDOW_GEOMETRY(this->display, this->window);
	return _y;
}

unsigned int
XlibWindow::w()
{
	GET_WINDOW_GEOMETRY(this->display, this->window);
	return _width;
}

unsigned int
XlibWindow::h()
{
	GET_WINDOW_GEOMETRY(this->display, this->window);
	return _height;
}

bool
XlibWindow::supports_alpha()
{
	GET_WINDOW_GEOMETRY(this->display, this->window);
	return _depth == TrueColor;
}

#undef GET_WINDOW_GEOMETRY

bool
XlibWindow::reconfigured()
{
	bool reconf = this->_reconfigured;
	this->_reconfigured = false;

	return reconf;
}

bool 
XlibWindow::closed()
{ return this->_closed; }

/* Prelude glue functions */
std::unique_ptr<window::IWindow>
window::create_window(unsigned int width, unsigned int height)
{
	return std::unique_ptr<window::IWindow>(
		new window::xlib::XlibWindow(NULL, width, height)
	);
}
