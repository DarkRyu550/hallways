#include "graphics.hh"

using namespace graphics;

Framebuffer::Framebuffer(int width, int height): width(width), height(height)
{
	/* Allocate data for new new frame */
	this->data = new T[height * width];
}

Framebuffer::~Framebuffer()
{
	/* Deallocate framebuffer data */
	delete this->data;
}

void
Framebuffer::clear(T value)
{
	for(int i = 0; i < this->height; ++i)
	for(int j = 0; j < this->width; ++j)
			this->data[i * this->height + j] = value;
}

bool
Framebuffer::in_bounds(int x, int y)
{
	return y * this->height + x 
		< this->height * this->width;
}

void
Raster::clear(int flags)
{
	/* Certify IEEE 754 compatibility */
	static_assert(std::numeric_limits<float>::is_iec559, "Target must be IEEE 754 compliant");

	if(flags & CLEAR_COLOR)
		this->color.clear({0, 0, 0});
	if(flags & CLEAR_DEPTH){
		static float minus_infinity = -std::numeric_limit<float>::infinity();
		this->depth.clear(minus_infinity);
	}
}

void
Raster::line(float x1, float y1, float x2, float y2, RGB color){
	float dx = x2 - x1;
	float dy = y2 - y1;
			
	float max_x = std::max(x1, x2);
	float min_x = std::min(x1, x2);
	float max_y = std::max(y1, y2);
	float min_y = std::min(y1, y2);

	if(dx == 0f){
		/* Vertical line */
		int x = (int) std::round(x1);
		int y;
		for(y = (int) std::round(min_y); y < (int) std::round(max_y); ++y)
			this->color[y * this->color.width() + x] = color;
	}else if(dy == 0f){
		/* Horizontal line */
	}else{
		float slope = dy / dx;
		
	
	}
}
