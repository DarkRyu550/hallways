#ifndef __INPUT_PRELUDE_HH__
#define __INPUT_PRELUDE_HH__

namespace input{
	class IPollDevice{
	public:
		/* Processes a single event, if any available.
		 * Returns immediately, if you're looking for a
		 * blocking function, use wait_event(), instead.
		 *
		 * Returns the number of events processed, or -1
		 * in case of an error, in which case, errno con-
		 * tains the error code.
		 */
		virtual int poll_event() = 0;

		/* Same as poll_event(), however, blocking the
		 * thread until an event becomes available */
		virtual int wait_event() = 0;

		/* Processes all available events. */
		virtual void saturate() = 0;
	};

	class IKeyboard: public IPollDevice{
	public: /* IPollDevice functions */
		virtual int poll_event() = 0;
		virtual void saturate() = 0;
	public:
		virtual int is_pressed(int keycode) = 0;
	};

	class IMouse: public IPollDevice{
	public: /* IPollDevice funtions */
		virtual int poll_event() = 0;
		virtual void saturate() = 0;
	public:
		virtual int dx() = 0;
		virtual int dy() = 0;
		virtual bool is_pressed(int button) = 0;
	};
}

#endif
