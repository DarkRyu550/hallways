#ifndef __AUDIO_HH__
#define __AUDIO_HH__

#include "prelude.hh"

#define AUDIO_METHOD_ALSA	1
#define AUDIO_METHOD_OSS	2

#if AUDIO_METHOD == AUDIO_METHOD_ALSA
#include "alsa.hh"
#endif

#if AUDIO_METHOD == AUDIO_METHOD_OSS
#include "oss.hh"
#endif

#endif
