#include "benchmark.hh"

#include "../math/math.hh"
void
mat4f_mul()
{
	static math::mat4f m0 = {{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}};
	static math::mat4f m1 = {{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}};
	static math::mat4f result;
	
	m0 * m1;
}

int
main()
{
	std::function<void(void)> a = mat4f_mul;
	std::string as = "mat4f_mul";
	bench::run(a, as);
	return 0;
}
